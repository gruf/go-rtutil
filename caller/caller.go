package caller

import (
	"runtime"
	"strings"
)

// Get fetches a human-readable calling function name with top-level package name, skipping given number of func frames.
func Get(skip int) string {
	var pcs [1]uintptr

	// Fetch pcs of callers
	n := runtime.Callers(skip+1, pcs[:])
	if n <= 0 {
		return ""
	}

	// Search for function at this PC
	fun := runtime.FuncForPC(pcs[0])
	if fun == nil {
		return ""
	}

	// Separate name before edits
	name := fun.Name()

	// Drop all but the package name and function name, no mod path
	if idx := strings.LastIndex(name, "/"); idx >= 0 {
		name = name[idx+1:]
	}

	const params = `[...]`

	// Drop any generic type parameter markers
	if idx := strings.Index(name, params); idx >= 0 {
		name = name[:idx] + name[idx+len(params):]
	}

	return name
}
