package structfields

import (
	"reflect"
	"sync"

	"codeberg.org/gruf/go-rtutil/v2"
)

var (
	// fieldsCache is a cache of reflected StructField slices stored
	// under the pointer value to each of their runtime value types.
	fieldsCache = map[uintptr][]reflect.StructField{}

	// fieldMutex protects fieldsCache.
	fieldsMutex = sync.Mutex{}
)

func init() {
	// Speed up the case of nil types by adding
	// in empty struct fields at nil ptr value.
	fieldsMutex.Lock()
	fieldsCache[0] = nil
	fieldsMutex.Unlock()
}

// Get will fetch struct field information for any struct-type 'v', caching the results
// between consecutive calls. For this reason you should treat returned fields as READ-ONLY. This
// method is useful if you expect to be regularly reflecting and iterating a type's struct fields.
func Get(v interface{}) []reflect.StructField {
	return GetByType(reflect.TypeOf(v))
}

// GetByType will fetch struct field information for any reflected struct-type 't', caching
// the results between consecutive calls. For this reason you should treat returned fields as READ-ONLY. This
// method is useful if you expect to be regularly reflecting and iterating a type's struct fields.
func GetByType(t reflect.Type) []reflect.StructField {
	// Get runtime type ptr of original type
	typeptr := rtutil.GetRuntimeType(t)
	uptr := uintptr(typeptr)

	// Acquire cache lock
	fieldsMutex.Lock()

	// Checked for cached []StructField
	fields, ok := fieldsCache[uptr]

	if ok {
		// Fast case, already calculated
		fieldsMutex.Unlock()
		return fields
	}

	// Track original ptrs
	o, ouptr := t, uptr

	// Repeatedly deref ptr types
	for t.Kind() == reflect.Ptr {
		t = t.Elem()
	}

	if t.Kind() != reflect.Struct {
		// Unhandleable type
		fieldsMutex.Unlock()
		return nil
	}

	if o != t {
		// This was dereferenced, so lets
		// recheck if this 'new' non-ptr
		// type's fields are cached
		typeptr = rtutil.GetRuntimeType(t)
		uptr = uintptr(typeptr)
		fields, ok = fieldsCache[uptr]

		if ok {
			// Add these fields under the
			// original r type's pointer
			fieldsCache[ouptr] = fields
			fieldsMutex.Unlock()
			return fields
		}

		// Note that uptr is now updated to
		// point to the deref'd struct type
	}

	// Calculate slice of struct fields from r type
	fields = make([]reflect.StructField, t.NumField())
	for i := 0; i < len(fields); i++ {
		fields[i] = t.Field(i)
	}

	// Add these fields to cache
	fieldsCache[uptr] = fields

	if ouptr != uptr {
		// Also add these fields under
		// original ptr type in cache
		fieldsCache[ouptr] = fields
	}

	fieldsMutex.Unlock()
	return fields
}
