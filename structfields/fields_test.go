package structfields_test

import (
	"encoding/json"
	"net/http"
	"reflect"
	"sync"
	"testing"

	"codeberg.org/gruf/go-rtutil/v2/structfields"
)

var structs = []interface{}{
	http.Client{},
	http.Server{},
	sync.Mutex{},
	http.Transport{},
	sync.RWMutex{},
	sync.Cond{},
	sync.Once{},
	json.Encoder{},
	json.Decoder{},
	&http.Client{},
	&http.Server{},
	&sync.Mutex{},
	&http.Transport{},
	&sync.RWMutex{},
	&sync.Cond{},
	&sync.Once{},
	&json.Encoder{},
	&json.Decoder{},
	nil,
	(interface{})(nil),
	(*http.Client)(nil),
	(*http.Server)(nil),
	(*sync.Mutex)(nil),
	(*http.Transport)(nil),
	(*sync.RWMutex)(nil),
	(*sync.Cond)(nil),
	(*sync.Once)(nil),
	(*json.Encoder)(nil),
	(*json.Decoder)(nil),
	(*interface{})(nil),
	(**http.Client)(nil),
	(**http.Server)(nil),
	(**sync.Mutex)(nil),
	(**http.Transport)(nil),
	(**sync.RWMutex)(nil),
	(**sync.Cond)(nil),
	(**sync.Once)(nil),
	(**json.Encoder)(nil),
	(**json.Decoder)(nil),
	(**interface{})(nil),
}

func TestGetStructFields(t *testing.T) {
	for _, v := range structs {
		fields := structfields.Get(v)
		rtype := reflect.TypeOf(v)

		// Repeatedly dereference pointer
		for rtype != nil && rtype.Kind() == reflect.Ptr {
			rtype = rtype.Elem()
		}

		// Log the result of dereffing
		t.Logf("%T => %v", v, rtype)

		// Check if this is a non-nil struct type, we don't need to
		// worry about nil because len(fields) SHOULD cover us in the
		// case of a nil rtype. If not then test is performing as it should
		if rtype != nil && rtype.Kind() != reflect.Struct {
			continue
		}

		for i := 0; i < len(fields); i++ {
			if !reflect.DeepEqual(fields[i], rtype.Field(i)) {
				t.Fatalf("unexpected struct field for type: %T", v)
			}
		}
	}
}

func TestGetStructFieldsByType(t *testing.T) {
	for _, v := range structs {
		rtype := reflect.TypeOf(v)
		fields := structfields.GetByType(rtype)

		// Repeatedly dereference pointer
		for rtype != nil && rtype.Kind() == reflect.Ptr {
			rtype = rtype.Elem()
		}

		// Log the result of dereffing
		t.Logf("%T => %v", v, rtype)

		// Check if this is a non-nil struct type, we don't need to
		// worry about nil because len(fields) SHOULD cover us in the
		// case of a nil rtype. If not then test is performing as it should
		if rtype != nil && rtype.Kind() != reflect.Struct {
			continue
		}

		for i := 0; i < len(fields); i++ {
			if !reflect.DeepEqual(fields[i], rtype.Field(i)) {
				t.Fatalf("unexpected struct field for type: %T %#vv", v, fields[i])
			}
		}
	}
}

func BenchmarkGetStructFields(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			for _, v := range structs {
				t := reflect.TypeOf(v)

				if t != nil && t.Kind() == reflect.Ptr {
					// Deref pointer
					t = t.Elem()
				}

				if t == nil || t.Kind() != reflect.Struct {
					// We only support struct values types
					continue
				}

				// Create a slice of struct fields from rtype
				fields := make([]reflect.StructField, t.NumField())
				for i := 0; i < len(fields); i++ {
					fields[i] = t.Field(i)
				}
			}
		}
	})
}

func BenchmarkGetStructFieldsCached(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			for _, v := range structs {
				_ = structfields.Get(v)
			}
		}
	})
}
