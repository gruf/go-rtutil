package rtutil

import (
	"reflect"
	"unsafe"
)

// Eface (empty interface) is the header for an interface{} value.
type Eface struct {
	// Type is a pointer to the runtime._type.
	Type unsafe.Pointer

	// Value is a pointer to the actual data.
	Value unsafe.Pointer
}

// ToEface will return the Eface header for given interface{} value.
func ToEface(v interface{}) *Eface {
	return (*Eface)(unsafe.Pointer(&v))
}

// GetRuntimeType will fetch a pointer for the runtime._type value of the
// originating object this reflect.Type represents. This is possible because
// the reflect package returns the value of an interface{}'s Eface.Type cast
// as their copy of runtime._type. So the Eface.Data value for this reflected
// type will be the underlying runtime type this was from.
func GetRuntimeType(t reflect.Type) unsafe.Pointer {
	if t == nil {
		return nil
	}
	return ToEface(t).Value
}
